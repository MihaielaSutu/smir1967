package biblioteca.repo;

import biblioteca.repository.repo.CartiRepo;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class Test3 {
    public static CartiRepo cartiRepo;

    @BeforeClass
    public static void instantiate() {
        cartiRepo = new CartiRepo();
    }

    @Test
    public void testValidGetCartiDinAnul() {
        assertTrue(cartiRepo.getCartiOrdonateDinAnul("1973").size()!=0);
    }

    @Test
    public void testInvalidGetCartiDinAnul() {
        assertTrue(cartiRepo.getCartiOrdonateDinAnul("2000").size()==0);
    }
}
