package biblioteca.repo;


import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class Test2 {

   public static CartiRepo cartiRepo;

   @BeforeClass
   public static void instantiate() {
       cartiRepo = new CartiRepo();
   }

   @Test
   public void testValidSearchBooksByAuthor1() {
       try {
           assertTrue(cartiRepo.cautaCarte("caragiale").size()!=0);
       } catch (Exception e) {
       }
   }

   @Test
   public void testInvalidSearchBooksByAuthor1() {
       try {
           assertTrue(cartiRepo.cautaCarte("ion carag2iale").size()==0);
       } catch (Exception e) {
           assertTrue(true);
       }
   }

   @Test
   public void testValidSearchBooksByAuthor2() {
       try {
           List<Carte> rez = cartiRepo.cautaCarte("caragiale");
           assertEquals(1, rez.size());
       } catch (Exception e) {
       }
   }

   @Test
   public void testValidSearchBooksByAuthor3() {
       try {
           List<Carte> rez = cartiRepo.cautaCarte("Vasilica");
           assertEquals(0, rez.size());
       } catch (Exception e) {
       }
   }

   @Test
   public void testInvalidSearchBooksByAuthor2() {
       try {
           List<Carte> rez = cartiRepo.cautaCarte("");
       } catch (Exception e) {
           assertTrue(true);
       }
   }
}

